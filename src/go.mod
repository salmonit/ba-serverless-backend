module moritz31/meal-lambda

go 1.12

require (
	github.com/aws/aws-lambda-go v1.10.0
	github.com/aws/aws-sdk-go v1.19.18
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/jinzhu/gorm v1.9.4
	github.com/liamylian/jsontime v1.0.1
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/ugorji/go v1.1.4 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
)
