package database

import (
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var (
	host = os.Getenv("DB_HOST")
	port = os.Getenv("DB_PORT")
	user = os.Getenv("DB_USER")
	name = os.Getenv("DB_NAME")
	pass = os.Getenv("DB_PASS")
)

func Open() (db *gorm.DB) {
	args := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", user, pass, host, port, name)
	// Initialize a new db connection.
	db, err := gorm.Open("mysql", args)
	if err != nil {
		panic(err)
	}
	return
}
