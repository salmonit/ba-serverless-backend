package models

import "github.com/jinzhu/gorm"

type Meal struct {
   gorm.Model
   Date string `json:"date"`
   Name  string `json:"name"`
   Price  string `json:"price"`
}