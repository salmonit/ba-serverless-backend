variable "database_user" {
  type = "string"
  default = "root"
}

variable "database_pass" {
  type = "string"
}

variable "database_cluster_name" {
  type = "string"
  default = "serverless-cluster"
}

data "aws_region" "current" {}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = "${aws_default_vpc.default.id}"

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_default_subnet" "default_az1" {
  availability_zone = "eu-central-1a"

  tags = {
    Name = "Default subnet for eu-central-1a"
  }
}

resource "aws_default_subnet" "default_az2" {
  availability_zone = "eu-central-1b"

  tags = {
    Name = "Default subnet for eu-central-1b"
  }
}

resource "aws_default_subnet" "default_az3" {
  availability_zone = "eu-central-1c"

  tags = {
    Name = "Default subnet for eu-central-1c"
  }
}

data "archive_file" "insert_lambda_code" {
  type        = "zip"
  source_file = "${path.module}/src/bin/insert"
  output_path = "${path.module}/insert.zip"
  depends_on  = ["null_resource.build_lambda"]
}

data "archive_file" "list_lambda_code" {
  type        = "zip"
  source_file = "${path.module}/src/bin/list"
  output_path = "${path.module}/list.zip"
  depends_on  = ["null_resource.build_lambda"]
}

resource "null_resource" "build_lambda" {
  triggers {
    build_number = "${timestamp()}"
  }

  provisioner "local-exec" {
    command     = " ${path.module}/build.sh"
    interpreter = ["bash", "-c"]
  }
}

resource "aws_lambda_function" "insert" {
  function_name    = "insert"
  handler          = "insert"
  filename         = "${data.archive_file.insert_lambda_code.output_path}"
  source_code_hash = "${data.archive_file.insert_lambda_code.output_base64sha256}"

  role        = "${aws_iam_role.lambdaRole.arn}"
  runtime     = "go1.x"
  memory_size = 128
  timeout     = 10

  vpc_config {
    subnet_ids         = ["${aws_default_subnet.default_az1.id}", "${aws_default_subnet.default_az2.id}", "${aws_default_subnet.default_az3.id}"]
    security_group_ids = ["${aws_default_security_group.default.id}"]
  }

  environment {
    variables = {
      DB_HOST = "${aws_rds_cluster.postgresql.endpoint}"
      DB_NAME = "${aws_rds_cluster.postgresql.database_name}"
      DB_USER = "${aws_rds_cluster.postgresql.master_username}"
      DB_PASS = "${aws_rds_cluster.postgresql.master_password}"
      DB_PORT = "3306"
    }
  }
}

resource "aws_lambda_function" "list" {
  function_name    = "list"
  handler          = "list"
  filename         = "${data.archive_file.list_lambda_code.output_path}"
  source_code_hash = "${data.archive_file.list_lambda_code.output_base64sha256}"

  role        = "${aws_iam_role.lambdaRole.arn}"
  runtime     = "go1.x"
  memory_size = 128
  timeout     = 10

  vpc_config {
    subnet_ids         = ["${aws_default_subnet.default_az1.id}", "${aws_default_subnet.default_az2.id}", "${aws_default_subnet.default_az3.id}"]
    security_group_ids = ["${aws_default_security_group.default.id}"]
  }

  environment {
    variables = {
      DB_HOST = "${aws_rds_cluster.postgresql.endpoint}"
      DB_NAME = "${aws_rds_cluster.postgresql.database_name}"
      DB_USER = "${aws_rds_cluster.postgresql.master_username}"
      DB_PASS = "${aws_rds_cluster.postgresql.master_password}"
      DB_PORT = "3306"
    }
  }
}

resource "aws_iam_role" "lambdaRole" {

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": {
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": "lambda.amazonaws.com"
    },
    "Effect": "Allow"
  }
}
POLICY
}

resource "aws_iam_role_policy_attachment" "attach_lambdaVPC" {
  role = "${aws_iam_role.lambdaRole.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
  
}


# Allow API gateway to invoke the hello Lambda function.
resource "aws_lambda_permission" "insert" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.insert.arn}"
  principal     = "apigateway.amazonaws.com"
}

resource "aws_lambda_permission" "list" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.list.arn}"
  principal     = "apigateway.amazonaws.com"
}

# A Lambda function is not a usual public REST API. We need to use AWS API
# Gateway to map a Lambda function to an HTTP endpoint.

resource "aws_api_gateway_rest_api" "hello" {
  name = "api"
}

resource "aws_api_gateway_resource" "list" {
  rest_api_id = "${aws_api_gateway_rest_api.hello.id}"
  parent_id   = "${aws_api_gateway_rest_api.hello.root_resource_id}"
  path_part   = "list"
}

resource "aws_api_gateway_resource" "list_month" {
  rest_api_id = "${aws_api_gateway_rest_api.hello.id}"
  parent_id = "${aws_api_gateway_resource.list.id}"
  path_part = "{month}"
}

resource "aws_api_gateway_resource" "list_year" {
  rest_api_id = "${aws_api_gateway_rest_api.hello.id}"
  parent_id = "${aws_api_gateway_resource.list_month.id}"
  path_part = "{year}"
}


#           GET
# Internet -----> API Gateway
resource "aws_api_gateway_method" "list" {
  rest_api_id   = "${aws_api_gateway_rest_api.hello.id}"
  resource_id   = "${aws_api_gateway_resource.list_year.id}"
  http_method   = "GET"
  authorization = "NONE"

  request_parameters {
    "method.request.path.month" = true
    "method.request.path.year" = true
  }
}

resource "aws_api_gateway_resource" "graphql" {
  rest_api_id = "${aws_api_gateway_rest_api.hello.id}"
  parent_id   = "${aws_api_gateway_rest_api.hello.root_resource_id}"
  path_part   = "insert"
}

#           GET
# Internet -----> API Gateway
resource "aws_api_gateway_method" "graphql" {
  rest_api_id   = "${aws_api_gateway_rest_api.hello.id}"
  resource_id   = "${aws_api_gateway_resource.graphql.id}"
  http_method   = "POST"
  authorization = "NONE"
}

#              POST
# API Gateway ------> Lambda
# For Lambda the method is always POST and the type is always AWS_PROXY.
#
# The date 2015-03-31 in the URI is just the version of AWS Lambda.
resource "aws_api_gateway_integration" "list" {
  rest_api_id             = "${aws_api_gateway_rest_api.hello.id}"
  resource_id             = "${aws_api_gateway_resource.list_year.id}"
  http_method             = "${aws_api_gateway_method.list.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/${aws_lambda_function.list.arn}/invocations"

  request_parameters {
    "integration.request.path.month" = "method.request.path.month"
    "integration.request.path.year" = "method.request.path.year"
  }
}

resource "aws_api_gateway_integration" "insert" {
  rest_api_id             = "${aws_api_gateway_rest_api.hello.id}"
  resource_id             = "${aws_api_gateway_resource.graphql.id}"
  http_method             = "${aws_api_gateway_method.graphql.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/${aws_lambda_function.insert.arn}/invocations"
}

# This resource defines the URL of the API Gateway.
resource "aws_api_gateway_deployment" "api" {
  depends_on = [
    "aws_api_gateway_integration.insert",
    "aws_api_gateway_integration.list",
  ]

  rest_api_id = "${aws_api_gateway_rest_api.hello.id}"
  stage_name  = "v1"
}

# Set the generated URL as an output. Run `terraform output url` to get this.

resource "aws_rds_cluster" "postgresql" {
  engine_mode             = "serverless"
  availability_zones      = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  database_name           = "meal"
  master_username         = "${var.database_user}"
  master_password         = "${var.database_pass}"
  backup_retention_period = 5
  preferred_backup_window = "07:00-09:00"
  skip_final_snapshot = true
}

output "api_endpoint" {
  value = "${aws_api_gateway_deployment.api.invoke_url}"
}

output "db_endpoint" {
  value = "${aws_rds_cluster.postgresql.endpoint}"
}

